import io

from setuptools import find_packages
from setuptools import setup

setup(
    name="devops",
    packages=find_packages(),
)